---
layout: handbook-page-toc
title: "Technical Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Technical Marketing at GitLab

![Golden Circle](/handbook/marketing/product-marketing/images/goldencircle.png)

#### Why (purpose)
Technical Marketing exists to create content meant to entice prospects into the top of the funnel by expressing the technical capabilities of the product as values to our target audiences.

#### How (process)
We do this by teaching our audiences about modern software delivery methods and how it can be valuable to them, introducing them to new concepts which can help them achieve their goals, and by showcasing the capabilities of the product for the use cases which our audiences care about.

#### What (output)
We produce demos, videos, workshops, tutorials, technical white papers, conference presentations, and webinars.

View the [Technical Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/926375?&label_name[]=tech-pmm) to see what the TMM team is working on.

##### Demos

Demos help show the value GitLab can bring to customers. Go to the [Demo page](/handbook/marketing/product-marketing/demo/) to see what's available and get more info.

### Which Technical Marketing Manager should I contact?

- Each TMM is listed with their areas of primary responsibility, but all TMM's should be able to help in other areas of the product as well:

  - [Tye Davis](/company/team/#TyeD19) - Agile, SCM (acting)
  - [Itzik Gan-Baruch](/company/team/#itzikgb), CI, CD (acting)
  - [Fernando Diaz](/company/team/#fjdiaz), DevSecOps, GitOps/IaC (acting)
  - [Hired! - starting in April](https://about.gitlab.com/jobs/apply/technical-marketing-manager-4380429002/) - SCM
  - [Hired! - starting in April](https://about.gitlab.com/jobs/apply/technical-marketing-manager-4380429002/) - CD/Release
  - [Open Vacancy](https://about.gitlab.com/jobs/apply/technical-marketing-manager-4380429002/) - GitOps/IaC
  - [Dan](/company/team/#dbgordon), Manager, TMM
  - [Ashish](/company/team/#kuthiala), Sr. Director, SM

### Technical Marketing Howto's
* Adding comparison pages ([instructions](/handbook/marketing/website/#creating-a-devops-tools-comparison-page), [video](https://youtu.be/LH4lKT-H2UU))
* Creating click-through demos
* Creating a Google (GCP) GKE cluster for GitLab demo
* [Creating an AWS EKS cluster for a GitLab demo](./howto/eks-cluster-for-demo.html)
