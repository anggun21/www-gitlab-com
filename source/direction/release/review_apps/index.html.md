---
layout: markdown_page
title: "Category Direction - Review Apps"
---

- TOC
{:toc}

## Review Apps

Review Apps let you build a review process right into your software development workflow
by automatically provisioning test environments for your code, integrated right
into your merge requests.

This ties into our [progressive delivery](https://about.gitlab.com/direction/cicd/#progressive-delivery) vision of CI/CD as it gives you a 
glipse of how  your application will look after a specific commit, way before it reaches production.

This area of the product is in need of continued refinement to add more kinds of
review apps (such as for mobile devices), and a smoother, easier to use experience.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AReview%20Apps)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/ci/review_apps/)

## What's Next & Why

Our current Review apps implementation requires a static URL that is used as the CI/CD variable. In many use cases, the environment variable is not static but dynamic, for example when using AWS, [gitlab#17066](https://gitlab.com/gitlab-org/gitlab/issues/17066) introduces a way to pass the environment names between jobs, allowing not only support for dynamic URLs for Review Apps but even more flexibility that can solve other dynamic needs of the CI/CD pipeline.


## Maturity Plan

This category is currently at the "Complete" maturity level, and
our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/#maturity-plan)).
Key deliverables to achieve this are:
- [Extend the API to provide an ability to run scripts to delete environment](https://gitlab.com/gitlab-org/gitlab/issues/5582) (Complete)
- [Optional expiration time for environments](https://gitlab.com/gitlab-org/gitlab/issues/20956) (Complete)
- [One button to enable review apps, auto-edit `.gitlab-ci.yml`, auto-configure GKE](https://gitlab.com/groups/gitlab-org/-/epics/2349)
- [Truly dynamic environment URLs](https://gitlab.com/gitlab-org/gitlab/issues/17066)
- [In-browser Review Apps for iOS/Android development](https://gitlab.com/gitlab-org/gitlab/issues/20295)
- [Mechanism to clean up stale environments](https://gitlab.com/gitlab-org/gitlab/issues/19724)
- [First-class review apps](https://gitlab.com/gitlab-org/gitlab/issues/18982)

## Visual Reviews
We recently added the ability to comment directly on the review apps [gitlab#10761](https://gitlab.com/gitlab-org/gitlab/issues/10761), With a small code snippet, users can enable designers, product managers, and other stakeholders to quickly provide feedback on a merge request without leaving the app.

## Competitive Landscape

One big advantage Heroku Review Apps have over ours is that they are easier to set up
and get running. Ours require a bit more knowledge and reading of documentation to make
this clear. We can make our Review Apps much easier (and thereby much more visible) by
implementing [One button to enable review apps, auto-edit `.gitlab-ci.yml`, auto-configure GKE](https://gitlab.com/groups/gitlab-org/-/epics/2349),
which does the heavy lifting of getting them working for you.

## Top Customer Success/Sales Issue(s)

Management of Review Apps can be a challenge, particularly in cleaning them up. To
highlight the severity of how this issue can grow, www-gitlab-com project has over
1,500 running stale environments at the time of writing this, with no clear easy way
to clean them up. There are two main items that look to address this challenge:

- [gitlab#20956](https://gitlab.com/gitlab-org/gitlab/issues/20956) builds in an expiration date for review apps, beyond which they will automatically be terminated (Delivered in 12.8).
- [gitlab#19724](https://gitlab.com/gitlab-org/gitlab/issues/19724) (also the 2nd customer issue) implements a way to clean up environments that either did not have an expiration date or were not terminated for other reasons.

Review Apps for GitLab Pages is a highly requested functionality [gitlab#16907](https://gitlab.com/gitlab-org/gitlab/issues/16907).

## Top Customer Issue(s)

Our current Review apps implementation, requires a static URL that is used as the CI/CD variable. In many use cases, the environment variable is not static but dynamic. For example when using AWS, a user will probably want to use the environment name based on the stage. 

[gitlab#17066](https://gitlab.com/gitlab-org/gitlab/issues/17066) introduces a way to pass the environment names between jobs, allowing not only support for dynamic URLs for Review apps but even more flexibility that can solve other dynamic needs of the CI/CD pipeline.

## Top Internal Customer Issue(s)

In some cases after a review app is not available for a specific Merge Request  
([gitlab#25351](https://gitlab.com/gitlab-org/gitlab/issues/25351)), adding some 
information to the user as to what action needs to be done in order to resolve this and
taker action directly from the MR itself, would help not only our customers, 
but also our own developers. 

## Top Vision Item(s)

Our focus for the vision is to bring Review Apps to mobile workflows via
[gitlab#20295](https://gitlab.com/gitlab-org/gitlab/issues/20295) - adding
support to Android/iOS emulators via the Review App will enable a whole new kind
of development workflow in our product, and make Review Apps even more valuable.



