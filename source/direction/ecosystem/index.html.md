---
layout: markdown_page
title: "Group Direction - Ecosystem"
---

- TOC
{:toc}

## Overview

GitLab's vision is to be the best [single application for every part of the DevOps toolchain](/handbook/product/single-application/), but there are numerous products and services out there 
that are key to productive contribute to a software project, but not something 
that GitLab is working on building.

For these needs, Ecosystem's mission is to foster GitLab not as a product, but 
as a platform and a community. Ecosystem supports our **Integrations** with 
other products, our **APIs** for connecting the application to external 
services, and our **GDK** and **Frontend Foundations** that our community uses 
to develop the application itself.

Our goal is to make integrating with, extending the functionality of, or developing 
on GitLab itself an easy, delightful experience. As with all things GitLab, 
we strongly believe that [Everyone can contribute](#contributing), and Ecosystem supports
that mission directly through its work.

## Categories

### Integrations

Integrations are places where the GitLab product where, _directly in the codebase_, 
it connects to features and services from other products. These integrations offer 
our customers a seamless experience between these products, and range from 
lightweight features like [Slack notifications](https://docs.gitlab.com/ee/user/project/integrations/slack.html) 
for projects, to deep and complex integrations with [Atlassian JIRA](https://docs.gitlab.com/ee/user/project/integrations/jira.html) 
that connect a wide array of functionality throughout the GitLab product.

Today, there are several ways to integrate into GitLab&mdash;by adding your 
integration directly to our codebase, by consuming our APIs directly, or by 
using a community library to connect your code to GitLab, and many products, 
tools, and services that have already integrated with GitLab can be found on 
our [partner integration page](https://about.gitlab.com/partners/integrate/).

Integrations will focus primarily on adding new integrations that are key to 
the needs of our enterprise customers, and providing guidance for 3rd party 
systems and services that are contributing new integrations for their products.

[Category Direction](/direction/ecosystem/integrations/) &middot; 
[Documentation](https://docs.gitlab.com/ee/integration/) &middot; 
[Epic](https://gitlab.com/groups/gitlab-org/-/epics/1515) &middot; 
[Open Issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AIntegrations)

### API

The GitLab APIs enable _external_ products and services to access 
GitLab data and functionality. These resources are developed by both the 
various [Groups and Categories](/handbook/product/categories/) at GitLab and as 
well as our community of contributors.

The GitLab API **category** provides guidance and governance for all the 
Groups inside of GitLab that are creating and maintaining our REST and GraphQL 
APIs. The goal of this effort is to define best practices and requirements 
for the development of our APIs to create a consistently great experience for 
those integrating with GitLab as a platform.

[Category Direction](/direction/ecosystem/api/) &middot; 
[Documentation](https://docs.gitlab.com/ee/api/) &middot; 
[Epic](https://gitlab.com/groups/gitlab-org/-/epics/2040) &middot; 
[Open Issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AAPIs%2FSDKs)

### GDK

The GitLab Development Kit (GDK) is a central tool for how developers contribute to GitLab. It provides a simple way to install, configure, and run a local development environment. This tool is key to the success of our contributor community, both internal GitLab Team Members and the broader community of contributors.

[Category Direction](/direction/ecosystem/gdk) &middot; 
[Project Home](https://gitlab.com/gitlab-org/gitlab-development-kit/) &middot; 
[Setup Documentation](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/set-up-gdk.md) &middot; 
[Open Issues](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues)

### FE/UX Foundations

The Frontend and UX Foundations work centers around resources and tools that allow Frontend engineers and UX designers to design and implement the UI of GitLab more effectively. This includes the [Pajamas framework](https://design.gitlab.com/), and frontend tooling such as our webpack implementation. The goal of these efforts are to make developing the GitLab UI straightforward, performant, and maintainable.

[Category Direction](/direction/ecosystem/frontend-ux-foundation) &middot;
[Pajamas Documentation](https://design.gitlab.com/)

## Themes

### Freedom of choice

We firmly believe that a [single application](https://about.gitlab.com/handbook/product/single-application/) 
can successfully serve all the needs of the entire DevOps lifecycle. However, 
we understand that there are a myriad of reasons that many customers can't 
adopt GitLab in this way.

Customers may have specific tools they are committed to using because of:

1. The cost of migrating off of it, because of either the volume of content to 
  migrate, the risk of errors during the migration, the cost of training, etc
1. The cost of creating _new_ integrations with other tools in their toolchain
1. Specific regulatory, security, or compliance needs they must be able to meet
1. Niche or unique functionality that isn't available in GitLab

Because of these realities, we believe that our customers should have the 
**freedom to choose their tools**, and use what makes the most sense for their 
business&mdash;and we will support that freedom as best we can.

### Flexibility and Extensibility

We'll never anticipate every possible use-case, nor can we afford to support the 
development of every possible integration. So to that end, our aim is to create 
flexible and extensible enough tools that those integrating with us can create 
anything they need. 

## Contributing

At GitLab, one of our values is that everyone can contribute. If you're looking 
tocontribute your own integration, or otherwise get involved with features in 
the Ecosystem area, [you can find open issues here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem).

Feel free to reach out to the team directly if you need guidance or want 
feedback on your work by pinging [@deuley](https://gitlab.com/deuley) or 
[@gitlab-ecosystem-team](https://gitlab.com/gitlab-org/ecosystem-team) on your 
open MR.

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

## Influences

We're inspired by other companies with rich, developer-friendly experiences like 
[Salesforce](https://developer.salesforce.com/), 
[Shopify](https://help.shopify.com/en/api/getting-started), 
[Twilio](https://www.twilio.com/docs/), 
[Stripe](https://stripe.com/docs/development), 
[GitHub](https://github.com/marketplace) and their [APIs](https://developer.github.com/v3/checks/).

A large part of the success of these companies comes from their enthusiasm 
around enabling developers to integrate, extend, and interact with their 
services in new and novel ways, creating a spirit of [collaboration](https://about.gitlab.com/handbook/values/#collaboration) 
and [diversity](https://about.gitlab.com/handbook/values/#collaboration) that 
simply can't exist any other way. 
