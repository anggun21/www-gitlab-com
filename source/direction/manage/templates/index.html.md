---
layout: markdown_page
title: "Category Direction - Templates"
---

- TOC
{:toc}

## Templates

Templates make it easy to setup a new project by starting from an existing one that already has all the required configuration, files, and boilerplate. GitLab provides a variety of templates as a starting point for creating new projects and it's also possible to contribute to these; administrators can even configure templates specific to your GitLab instance.

We already have dozens of templates for common implementations: [Project templates](https://gitlab.com/gitlab-org/project-templates), [CI YAML templates](https://gitlab.com/gitlab-org/gitlab/tree/master/lib/gitlab/ci/templates), [Pages templates](https://gitlab.com/pages), and even [example projects](https://gitlab.com/gitlab-examples). Don't see a template that you wish we had? Please let us know! Contact [Haris Delalic](https://gitlab.com/hdelalic), the PM for this category.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Category%3ATemplates)
- [Overall Vision](/direction/dev/#manage-1)
- UX Research (TBD)
- Documentation: [Project templates](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#project-templates), [Custom group-level project templates](https://docs.gitlab.com/ee/user/group/custom_project_templates), [CI YAML templates](https://docs.gitlab.com/ee/ci/examples/README.html#cicd-examples)

## What's Next & Why

TBD

## Maturity Plan

This category is currently at the “Viable” maturity level, and our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)). Key deliverables to achieve this are:

- [Parameterize Project Templates](https://gitlab.com/gitlab-org/gitlab/issues/26580)
- [Document how to vendor Project Templates](https://gitlab.com/gitlab-org/gitlab/issues/22145)

## Competitive Landscape

### GitHub

GitHub offers ["Starter Workflows"](https://github.com/actions/starter-workflows) to get started with GitHub Actions. These are [templates for popular continuous integration workflows written in YAML](https://github.com/actions/starter-workflows/tree/master/ci). The YAML file is dependent on a corresponding .properties.json file that defines the metadata about the workflow. 

### Jenkins

Jenkins' concept of ["pipeline template"](https://jenkinsci.github.io/templating-engine-plugin/pages/Pipeline_Templating/what_is_a_pipeline_template.html) involves two other components, libraries and configuration files. The pipeline template defines "stages" which depend on "libraries" to specify the jobs to be executed for each stage, while the configuration file specifies which libraries are used. Reusable templates are consolidated and managed under a governance structure using [Jenkins Templating Engine (JTE)](https://jenkinsci.github.io/templating-engine-plugin/), a templating plugin.

### CircleCI

CircleCI provides sample configuration files ([Sample 2.0 config.yml Files](https://circleci.com/docs/2.0/sample-config/)) to guide the user, along with CircleCI Orbs' importable jobs as building blocks for creating a configuration file.

## Top Customer Success/Sales Issue(s)

There are currently no top CS/Sales issues for this category.

## Top Customer Issue(s) 

A top customer issue is a specific customer request to parameterize project templates ([gitlab#26580](https://gitlab.com/gitlab-org/gitlab/issues/26580)) to enable project templates configured with variables to prompt the user for inputs that will eliminate manual customizations after the project is created.

## Top Internal Customer Issue(s)

Our top internal customer issues include the following:

- Allow specifying environment variables for new templates ([gitlab#26979](https://gitlab.com/gitlab-org/gitlab/issues/26979))
- Allow specifying one-time script for new templates ([gitlab#26980](https://gitlab.com/gitlab-org/gitlab/issues/26980))
- Improve the workflow for creating a vendor template ([gitlab#22145](https://gitlab.com/gitlab-org/gitlab/issues/22145)) 

## Top Vision Item(s)

TBD
